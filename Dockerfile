FROM openjdk:8-jre-alpine

EXPOSE 8080

VOLUME /tmp

ADD target/ms-hackerthorn-1.0.0.jar ms-hackerthorn.jar

RUN /bin/sh -c 'touch /ms-hackerthorn.jar'

ENV TZ=Africa/Nairobi

ENV NO_PROXY="*.safaricom.net,.safaricom.net"

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ENTRYPOINT ["java","-Xmx256m", "-XX:+UseG1GC", "-Djava.security.egd=file:/dev/./urandom","-jar","/ms-hackerthorn.jar"]
