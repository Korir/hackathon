package com.safaricom.hackerthon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hackerthon22Application {

	public static void main(String[] args) {
		SpringApplication.run(Hackerthon22Application.class, args);
	}

}
