package com.safaricom.hackerthon.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.safaricom.hackerthon.controller.Entity.Addresses;
import com.safaricom.hackerthon.controller.Entity.Customer;
import com.safaricom.hackerthon.controller.Entity.Sequences;
import com.safaricom.hackerthon.controller.repository.AddressesRepository;
import com.safaricom.hackerthon.controller.repository.CustomerRepository;
import com.safaricom.hackerthon.controller.repository.SequencesRepository;
import com.safaricom.hackerthon.payload.ServiceResponse;
import com.safaricom.hackerthon.utils.LogManager;
import com.safaricom.hackerthon.utils.Validation;

@Service
public class CustomerServiceImpl implements CustomerService {

	CustomerRepository customerRepository;

	AddressesRepository addressesRepository;

	SequencesRepository sequencesRepository;

	@Autowired
	public CustomerServiceImpl(CustomerRepository customerRepository, AddressesRepository addressesRepository,
			SequencesRepository sequencesRepository) {
		this.customerRepository = customerRepository;
		this.addressesRepository = addressesRepository;
		this.sequencesRepository = sequencesRepository;
	}

	/*
	 * get A customer by ID
	 * 
	 */
	@Override
	public Customer getCustomer(String id) {
		LogManager.logInformation("getCustomer", " Customer ID " + id);
		if (id != null) {
			Optional<Customer> OptionalCustomer = this.customerRepository.findById(id);
			if (OptionalCustomer.isPresent()) {
				LogManager.logInformation("getCustomer", "Customer Data retreived " + id);
				return OptionalCustomer.get();
			} else
				return null;
		}
		LogManager.logInformation("getCustomer", "Empty Customer ID ");
		return null;
	}

	/*
	 * GET All customers by name
	 */
	public List<Customer> getCustomerByName(String name) {
		LogManager.logInformation("getCustomer", " Customer ID " + name);
		if (name != null) {
			List<Customer> optionalCustomer = this.customerRepository.findAllByName(name);
			return optionalCustomer;
		}
		LogManager.logInformation("getCustomer", "Empty Customer ID ");
		return null;
	}

	/*
	 * GET All customers by nationality
	 */
	@Override
	public List<Customer> getCustomerByNationality(String nationality) {
		LogManager.logInformation("getCustomer", " Customer nationality " + nationality);
		if (nationality != null) {
			List<Customer> optionalCustomer = this.customerRepository.findAllByNationality(nationality);
			return optionalCustomer;
		}
		LogManager.logInformation("getCustomer", "Empty Customer nationality ");
		return null;
	}

	/*
	 * create a new customer
	 * 
	 */
	@Override
	public ServiceResponse createCustomer(Customer customer) {

		LogManager.logInformation("createCustomer", customer.getId());
		String serverMessage = "";

		if (customer.getId() == null) {
			customer.setId(UUID.randomUUID().toString());
			customer.setActive(true);
			customer.setDateCreated(new Date());
		} else {
			/*
			 * Check if provided Customer Id Exists if not create new Customer
			 * 
			 */
			if (!customerRepository.findById(customer.getId()).isPresent()) {
				LogManager.logInformation("createCustomer", " Customer present : " + customer.getId());
				customer.setId(UUID.randomUUID().toString());
				customer.setDateCreated(new Date());
			}
		}

		/*
		 * validate mandatory fields name,surname,age,height,addresses.type,active
		 * 
		 */
		boolean validateName = Validation.validateNullAndEmptyStr(customer.getName());
		boolean validateSurname = Validation.validateNullAndEmptyStr(customer.getSurname());
		boolean validateHeight = Validation.validateNullAndEmptyDouble(customer.getHeight());
		boolean validateAge = Validation.validateNullAndEmptyInt(customer.getAge());

		if (!validateName)
			serverMessage = " Invalid Name ";
		if (!validateSurname)
			serverMessage = " Invalid Surname";
		if (!validateHeight)
			serverMessage = " Invalid Heigh must be greater than 0.0";
		if (!validateAge)
			serverMessage = " Invalid Age must be greater than 0";

		LogManager.logInformation("createCustomer", "Validation " + serverMessage);
		if (validateName && validateSurname && validateHeight && validateAge) {
			/*
			 * Save Addresses for the selected contact First check if customer has addresses
			 * each address should have a type
			 */
			if (customer.getAddresses() != null && customer.getAddresses().size() > 0) {
				List<Addresses> addresses = new ArrayList<Addresses>();
				for (Addresses address : customer.addresses) {
					address.setId(UUID.randomUUID().toString());
					if (Validation.validateNullAndEmptyStr(address.getType())) {
						address.setSequenceNo(getSerialNo());
						addresses.add(addressesRepository.saveAndFlush(address));
					} else
						return new ServiceResponse("Address Type is required", 402, null);
				}
				customer.setAddresses(addresses);
				customer.setLastUpdate(new Date());
				this.customerRepository.save(customer);
				LogManager.logInformation("createCustomer", "Customer Created Successfully " + customer.getId());
				return new ServiceResponse("Customer Created Successfully", 200, null);
			} else {
				LogManager.logInformation("createCustomer", "Customer Addresses required " + customer.getId());
				return new ServiceResponse("Customer Addresses required", 402, null);
			}

		} else
			return new ServiceResponse(serverMessage, 402, null);
	}

	/*
	 * Get All Customers
	 */
	@Override
	public List<Customer> getAllCustomer() {
		LogManager.logInformation("getAllCustomer", "All customers");
		return this.customerRepository.findAll();
	}

	/*
	 * Get Customers Address By Id
	 */
	@Override
	public Addresses getAddresses(String customerId, String id) {
		LogManager.logInformation("getAddresses", "get customer " + id + " address");
		return this.addressesRepository.findById(id).get();
	}

	/*
	 * Get All Customers Address By Id
	 */
	@Override
	public List<Addresses> getCustomerAddress(String id) {
		LogManager.logInformation("getCustomerAddress", "get All customer " + id + " addresses");
		return getCustomer(id).getAddresses();
	}

	public String getSerialNo() {
		Sequences sequences = sequencesRepository.findById(1).get();
		int current = sequences.getSequenceNo();
		sequences.setLastUpdated(new Date());
		sequences.setSequenceNo(current + 1);
		sequencesRepository.save(sequences);
		return sequences.getPrefix() + "-" + current + "-" + sequences.getSufix();
	}

	/*
	 * 
	 * Activate/Deactivate(delete) Customer
	 */
	@Override
	public ServiceResponse deActivateCustomer(String customerId, String status) {
		Optional<Customer> optionalCustomer = this.customerRepository.findById(customerId);
		if (optionalCustomer.isPresent()) {
			Customer customer = optionalCustomer.get();
			if (status.equals("activate"))
				customer.setActive(true);
			if (status.equals("deActivate"))
				customer.setActive(false);
			customerRepository.save(customer);
			return new ServiceResponse("Customer Updated Successfully", 200, null);
		} else
			return new ServiceResponse("Invalid Customer", 402, null);

	}

}
