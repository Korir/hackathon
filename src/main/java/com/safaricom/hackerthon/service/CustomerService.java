package com.safaricom.hackerthon.service;

import java.util.List;

import com.safaricom.hackerthon.controller.Entity.Addresses;
import com.safaricom.hackerthon.controller.Entity.Customer;
import com.safaricom.hackerthon.payload.ServiceResponse;

public interface CustomerService {

	Customer getCustomer(String id);

	ServiceResponse createCustomer(Customer customer);

	List<Customer> getAllCustomer();

	List<Addresses> getCustomerAddress(String id);

	
	Addresses getAddresses(String customerId, String id);

	ServiceResponse deActivateCustomer(String customerId,String status);

	List<Customer> getCustomerByNationality(String nationality);

}
