package com.safaricom.hackerthon.utils;

import java.util.logging.*;

public class LogManager {

	 private static Logger logger = Logger.getLogger("com.safaricom.hackerthon");
	 
	 public static void logInformation(String transactionType,String transactionMessage) {
		 logger.log(Level.WARNING, transactionType, transactionMessage);
	 }
	
}
