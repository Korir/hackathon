package com.safaricom.hackerthon.utils;

public class Validation {

	public static boolean validateNullAndEmptyStr(String value) {
		try {
			if (value != null && !value.isEmpty())
				return true;
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public static boolean validateNullAndEmptyInt(int value) {
		try {
			if (value > 0)
				return true;
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public static boolean validateNullAndEmptyDouble(double value) {
		try {
			System.out.print("===>"+(value > 0.0));
			if (value > 0.0)
				return true;
		} catch (Exception e) {
			return false;
		}
		return false;
	}
}
