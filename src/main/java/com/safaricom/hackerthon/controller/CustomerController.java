package com.safaricom.hackerthon.controller;

import java.util.ArrayList;
import java.util.List;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.safaricom.hackerthon.controller.Entity.Addresses;
import com.safaricom.hackerthon.controller.Entity.Customer;
import com.safaricom.hackerthon.payload.ServiceResponse;
import com.safaricom.hackerthon.service.CustomerService;

@RestController
@RequestMapping("customers")
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@GetMapping("{customerId}")
	public Customer getCustomer(@PathVariable String customerId) {
		return customerService.getCustomer(customerId);
	}

	@GetMapping(produces = { "application/hal+json" })
	public CollectionModel<Customer> getAllCustomer() {
		List<Customer> allCustomers = customerService.getAllCustomer();
		CollectionModel<Customer> result = getCustomerData(allCustomers);
		return result;
	}

	public CollectionModel<Customer> getCustomerData(List<Customer> allCustomers) {
		for (Customer customer : allCustomers) {
			String customerId = customer.getId();
			Link selfLink = linkTo(CustomerController.class).slash(customerId).withSelfRel();
			customer.add(selfLink);
			if (customer.getAddresses().size() > 0) {
				Link addresssLink = linkTo(methodOn(CustomerController.class).getaddresssForCustomer(customerId))
						.withRel("allAddresss");
				customer.add(addresssLink);
			}
			Link updateLink = linkTo(methodOn(CustomerController.class).updateCustomer(customerId, customer))
					.withRel("updateCustomerInfo");

			Link statusLink = null;
			if (!customer.active)
				statusLink = linkTo(methodOn(CustomerController.class).deActivate(customerId, "activate"))
						.withRel("CustomerDelete");

			if (customer.active)
				statusLink = linkTo(methodOn(CustomerController.class).deActivate(customerId, "deActivate"))
						.withRel("CustomerInfoDelete");

			customer.add(statusLink);
			customer.add(updateLink);
		}


		List<Link> link=	new ArrayList<>();
		link.add(linkTo(CustomerController.class).withSelfRel());
		link.add(linkTo(methodOn(CustomerController.class).getCustomersByNationality("KENYAN")).withSelfRel());
		link.add(linkTo(methodOn(CustomerController.class).postCustomer(new Customer())).withSelfRel());
		
		CollectionModel<Customer> result = CollectionModel.of(allCustomers, link);
		return result;
	}

	@GetMapping("/{nationality}/all-by-nationality")
	public CollectionModel<Customer> getCustomersByNationality(@PathVariable final String nationality) {
		List<Customer> allCustomers = customerService.getCustomerByNationality(nationality);
		CollectionModel<Customer> result = getCustomerData(allCustomers);
		return result;
	}

	@GetMapping(value = "/{customerId}/addresss", produces = { "application/hal+json" })
	public CollectionModel<Addresses> getaddresssForCustomer(@PathVariable final String customerId) {
		final List<Addresses> addresss = customerService.getCustomerAddress(customerId);
		for (final Addresses address : addresss) {
			final Link selfLink = linkTo(methodOn(CustomerController.class).getaddressById(customerId, address.getId()))
					.withSelfRel();
			address.add(selfLink);
		}
		Link link = linkTo(methodOn(CustomerController.class).getaddresssForCustomer(customerId)).withSelfRel();
		CollectionModel<Addresses> result = CollectionModel.of(addresss, link);
		return result;
	}

	@GetMapping("/{customerId}/{addressId}")
	public Addresses getaddressById(@PathVariable final String customerId, @PathVariable final String addressId) {
		return customerService.getAddresses(customerId, addressId);
	}

	@GetMapping("/{customerId}/{status}/delete")
	public ServiceResponse deActivate(@PathVariable String customerId, @PathVariable String status) {
		return this.customerService.deActivateCustomer(customerId, status);
	}

	@PostMapping("/{customerId}/update")
	public ServiceResponse updateCustomer(@PathVariable String customerId, @RequestBody Customer customer) {
		return this.customerService.createCustomer(customer);
	}

	@PostMapping("create")
	public ServiceResponse postCustomer(@RequestBody Customer customer) {
		return this.customerService.createCustomer(customer);
	}

}
