package com.safaricom.hackerthon.controller.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.safaricom.hackerthon.controller.Entity.Addresses;
import com.safaricom.hackerthon.controller.Entity.Customer;

public interface AddressesRepository extends JpaRepository<Addresses, String>  {

}
