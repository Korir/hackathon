package com.safaricom.hackerthon.controller.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.safaricom.hackerthon.controller.Entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, String> {

	List<Customer> findByName(String name);

	List<Customer> findAllByName(String name);

	List<Customer> findAllByNationality(String nationality);

}
